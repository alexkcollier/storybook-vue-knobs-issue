import SButton from './SButton.vue'
import { boolean, select, text, withKnobs } from '@storybook/addon-knobs'
import { storiesOf } from '@storybook/vue'

storiesOf('SButton JSX', module)
  .addDecorator(withKnobs)
  .add('Button JSX', () => ({
    props: {
      buttonColor: {
        type: String,
        default: select('Button color', ['green', 'red'], 'green', 'Optional Props')
      },

      buttonSize: {
        type: String,
        default: select('Button size', ['small', 'regular', 'large'], 'small', 'Optional Props')
      },

      buttonText: {
        type: String,
        default: text('Button text', 'Sample text', 'Slots')
      },

      disabled: {
        type: Boolean,
        default: boolean('disabled', false, '$attrs')
      }
    },

    render(h) {
      const { buttonText, disabled, ...props } = this.$props

      return (
        <div>
          <SButton {...{ props }} disabled={disabled}>
            {buttonText}
          </SButton>
        </div>
      )
    }
  }))
