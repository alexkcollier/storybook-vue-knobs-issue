const sharedConf = require('./config/shared-webpack-conf')

module.exports = {
  productionSourceMap: false,
  configureWebpack: {
    resolve: {
      alias: sharedConf.aliases
    }
  }
}
