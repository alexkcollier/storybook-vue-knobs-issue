module.exports = {
  presets: ['@vue/app'],
  plugins: ['transform-vue-jsx', '@babel/plugin-transform-runtime']
}
