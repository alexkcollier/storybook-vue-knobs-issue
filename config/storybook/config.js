/* eslint-disable import/no-extraneous-dependencies */
import SButton from '@/components/SButton'
import { addDecorator, configure } from '@storybook/vue'
import Vue from 'vue'

Vue.component(SButton)

const componentStories = require.context('@/components', false, /.stories.(js|jsx)$/)

function loadStories() {
  componentStories.keys().forEach(filename => componentStories(filename))
}

// is wrapped, but is not reactive
// addDecorator(story => ({
//   template: '<div id="template-div" style="padding: 3em; background: yellow"><story /></div>',
// }))

// is wrapped, but is not reactive
// addDecorator(() => ({
//   render(h) {
//     return <div style="padding: 3em; background: orange"><story /></div>
//   }
// }))

// is reactive, but is not wrapped
// addDecorator(story => {
//   const options = story()
//   return {
//     template: `
//       <div id="render div" style="padding: 3em; background: blue">
//         ${options.template}
//       </div>
//     `,
//     ...options
//   }
// })



configure(loadStories, module)
